# -*- coding: utf-8 -*-
"""
Module to help parse an article object from HTML

IMPORTANT: Make sure you have the 'html5lib' parser installed
"""
import bs4
import requests
import markdown


class Article(object):
    """
    A class to represent an article on the web
    """

    def __init__(self):
        self._id = None
        self.id = None
        self.title = None
        self.author = None
        self.publish_date = None
        self.parse_date = None
        self.body = None
        self.url = None

    def get_markdown(self):
        lines = [self.title, '=' * len(self.title), '', '*Original Source* - [%s](%s)' % (self.url, self.url), '', self.body]
        return '\n'.join(lines)

    def get_html(self):
        md = self.get_markdown()
        html = markdown.markdown(md)
        return html

    def to_json(self):
        json = {'id': self.id,
                'url': self.url,
                'title': self.title,
                'author': self.author,
                'publish_date': self.publish_date,
                'parse_date': self.parse_date,
                'body': self.body}
        if self._id:
            json['_id'] = self._id
        return json

    @classmethod
    def from_json(cls, json):
        article = Article()
        article._id = json['_id']
        article.id = json['id']
        article.url = json['url']
        article.title = json['title']
        article.author = json['author']
        article.publish_date = json['publish_date']
        article.parse_date = json['parse_date']
        article.body = json['body']
        return article


def get_article_from_url(url):
    """
    Constructs and returns and Article instance based on a url
    """
    response = requests.get(url)
    if response.status_code == 200:
        html_soup = bs4.BeautifulSoup(response.text)
        content_soups = find_main_content(html_soup)
        title = html_soup.find('title').get_text().strip().replace('\n', ' ')

        article = Article()
        article.url = url
        article.title = title
        markdowns = []
        for soup in content_soups:
            markdowns.append(get_markdown(soup))
        article.body = '\n\n'.join(markdowns)

        return article
    else:
        raise Exception('Received status code %s for %s' % (response.status_code, url))


def find_main_content(html_soup):
    """
    Tries to find a sub-node of the html tree which has the most
    real content, hoping to end up excluding any distractions from
    the page
    """

    # Getting the content weight of each node
    weights = {}
    nodes = {}
    get_content_weights(html_soup, weights, nodes)

    # Figuring out the max
    max_weight = 0
    max_key = None
    for key, weights in weights.iteritems():
        if sum(weights) > max_weight:
            max_weight = sum(weights)
            max_key = key

    # Deciding to send subtree or full
    if max_weight > 2:
        return nodes[max_key]
    else:
        return [html_soup]


def combine_section(sections):
    text = ''
    for i, section in enumerate(sections):
        text += section[1]

        if i < len(sections)-1:
            if section[0] == 'i':
                text += ' '
            else:
                text += '\n\n'
    return text


def count_tags(soup, tag):
    """
    Counts the number of tags that exist as the direct child of the soup
    """
    try:
        found = soup.find_all(tag, recursive=False)
        return len(found)
    except:
        return 0


def get_content_weights(soup, weights, nodes):
    """
    Traverses the soup tree and counts the weight of the direct content in
    each node. The hope is to be able to pin-point the node having the main
    content
    """
    for child in soup.children:
        if not isinstance(child, bs4.NavigableString):
            cls = ','.join(child.get('class', []))
            key = (child.name, child.get('id'), cls)
            weight = count_tags(child, 'p')
            if key in weights:
                weights[key].append(weight)
                nodes[key].append(child)
            else:
                weights[key] = [weight]
                nodes[key] = [child]
            get_content_weights(child, weights, nodes)


def get_markdown(soup, parents=[]):
    """
    Convert a beautiful soup node to markdown
    """

    if isinstance(soup, bs4.NavigableString):
        text = unicode(soup)
        if text.startswith('<!--') or text.endswith('-->'):
            return ''
        else:
            return text.strip()
    else:
        sections = []
        for child in soup.children:
            parents.append(soup.name)
            text = get_markdown(child, parents)
            section = None
            if not isinstance(child, bs4.Comment):
                if child.name not in ['cite', 'script', 'header', 'aside', 'figure', 'style']:
                    if child.name == 'b':
                        section = ('i', '**%s**' % text)
                    elif child.name in ['i', 'em']:
                        section = ('i', '*%s*' % text)
                    elif child.name == 'a' and 'h1' not in parents:
                        section = ('i', '[%s](%s)' % (text, child.get('href')))
                    elif child.name == 'p':
                        section = ('b', text)
                    elif child.name in ['h1', 'h2', 'h3', 'h4', 'h5']:
                        n = int(child.name[1])
                        section = ('b', '%s %s' % (n*'#', text))
                    elif child.name == 'li' and len(text) > 1:
                        if child.parent.name == 'ul':
                            section = ('b', '  * %s' % text)
                        else:
                            section = ('b', '  1. %s' % text)
                    elif child.name in ['ul', 'ol', 'div']:
                        section = ('b', text)
                    else:
                        section = ('i', text)

            if section and len(section[1]) > 0:
                sections.append(section)

        return combine_section(sections)


def get_article(soup):
    """
    Gets the article section from html
    @param soup: Beautiful Soup instance of the html
    """

    # First try to find and html5 article
    for article in soup.find_all('article'):
        return article

    # Then try to find a div with class 'post'
    for div in soup.find_all('div', {'class': 'post'}):
        return div

    return soup.body
