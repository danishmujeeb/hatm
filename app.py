import urlparse
import logging

from flask import Flask, render_template, request, Response, jsonify, redirect
from flask.ext.mail import Mail, Message

import ids
import hatm
import share
import config
from store import ArticleStore


app = Flask(__name__)

# Configuring the application
app.config.update(
    MAIL_SERVER=config.email_server,
    MAIL_USE_TLS=True,
    MAIL_USERNAME=config.email_account,
    MAIL_PASSWORD=config.email_password,
    DEFAULT_MAIL_SENDER=config.email_account
)

mail = Mail(app)

store = ArticleStore(config.mongo_host, config.mongo_port, config.mongo_user, config.mongo_password)

@app.route('/')
def home():
    return render_template('home.html')


@app.route('/r/<shortname>')
def article_by_shortname(shortname):
    id = ids.decode_id(shortname)
    render_format = request.args.get('format', '')

    original_article = store.article_by_id(id)
    article = hatm.get_article_from_url(original_article.url)
    article.id = original_article.id
    store.update(article)

    return render_article(article, render_format)


@app.route('/c/<shortname>')
def compare_article(shortname):
    id = ids.decode_id(shortname)
    render_format = request.args.get('format', '')

    article = store.article_by_id(id)

    return render_template('compare.html', left_url=article.url, right_url='/r/%s' % shortname)


@app.route('/s/<shortname>/<platform>')
def share_article(shortname, platform):
    id = ids.decode_id(shortname)
    article = store.article_by_id(id)
    if platform == 'email':
        address = request.args.get('email_address', '')
        msg = Message(subject=article.title, recipients=[address], sender=config.email_account,
                      body=article.get_markdown(), html=article.get_html())
        mail.send(msg)
        return "ok"


@app.route('/process')
def process():
    url = request.args.get('url', '')
    update = request.args.get('update', '')
    article = store.article_by_url(url)
    if article is None:
        article = hatm.get_article_from_url(url)
        article.id = store.next_id()
        store.save(article)
    else:
        logging.debug('Found existing article %s for url %s' % (article.id, url))
        if update == 'true':
            article.parse(url)
            store.update(article)

    return redirect('/r/%s' % ids.encode_id(article.id))



@app.route('/test')
def test():
    try:
        url = request.args.get('url', '')
        render_format = request.args.get('format', '')
        article = hatm.get_article_from_url(url)

        return render_article(article, render_format)
    except Exception as e:
        print 'Error processing url: %s' % url
        raise e


@app.route('/testmail')
def test_mail():
    msg = Message("Hello", sender="articles@motfy.io", recipients=["danish.mujeeb@gmail.com"])
    mail.send(msg)

def render_article(article, render_format):
    """
    Render article given a format
    """
    if render_format in ['md', 'text']:
        return render_article_as_text(article)
    else:
        return render_article_as_html(article)


def render_article_as_html(article):
    """
    Render article as html
    """
    src_domain = urlparse.urlparse(article.url)
    if src_domain:
        src_domain = src_domain.hostname

    return render_template('article.html', article=article.get_html(), src_domain=src_domain,
                           page_title=article.title, shortname=ids.encode_id(article.id))


def render_article_as_text(article):
    """
    Render article as text
    """
    return Response(article.get_markdown(), mimetype='text/plain')


if __name__ == '__main__':

    # Configuring the session


    logging.basicConfig(level=logging.DEBUG)
    app.run(host='0.0.0.0', debug=True)

