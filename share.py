import smtplib
import config
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def email(address, article):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = article.title
    msg['From'] = 'danish.mujeeb@live.com'
    msg['To'] = address

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(article.get_markdown().encode('utf-8'), 'plain')
    part2 = MIMEText(article.get_html().encode('utf-8'), 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via local SMTP server.
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login("danish.mujeeb@gmail.com", "Pa55wor|)")
    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    s.sendmail(config.email_from, address, msg.as_string())
    s.close()

