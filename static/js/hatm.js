
function emailArticle(shortName, emailAddress) {
    $("#email-button").attr("disabled", "disabled");
    var url = "/s/" + shortName + "/email?email_address=" + emailAddress;
    $.ajax({
       url: url,
       success: function(data) {
            $("#email-button").removeAttr("disabled");
       }
    });
}