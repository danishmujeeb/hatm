import requests
import hatm
import bs4

HTML = """<h1>Heading</h1>
<p> This is a paragreaph </p>
"""

def test1():

    r = requests.get('http://www.cnbc.com/id/101418139')
    #r = requests.get('http://www.bloomberg.com/news/2014-02-13/russians-uniting-around-sochi-offer-putin-return-on-games.html')

    soup = bs4.BeautifulSoup(r.text)
    weights = {}
    hatm.get_content_weights(soup, weights)

    for key, weights in weights.iteritems():
        if sum(weights) > 0:
            print key, weights


def test2():
    article = hatm.get_article_from_url('http://www.bloomberg.com/news/2014-02-13/russians-uniting-around-sochi-offer-putin-return-on-games.html')

    print article.get_markdown()

test1()