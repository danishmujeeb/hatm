import hatm
import pymongo


class ArticleStore(object):

    def __init__(self, host, port, user, password):
        self.client = pymongo.MongoClient(host, port)
        self.db = self.client['hatm']
        self.db.authenticate(user, password)


    def next_id(self):
        ret = self.db.counters.find_and_modify(query={'_id': 'urlid'},
                                               update={'$inc': {'seq': 1}},
                                               new=True)
        return int(ret['seq'])

    def save(self, article):
        self.db.articles.insert(article.to_json())

    def update(self, article):
        self.db.articles.update({'_id': article._id}, article.to_json(), True)

    def article_by_id(self, id):
        json = self.db.articles.find_one({'id': id})
        if json:
            return hatm.Article.from_json(json)

    def article_by_url(self, url):
        json = self.db.articles.find_one({'url': url})
        if json:
            return hatm.Article.from_json(json)




if __name__ == '__main__':
    store = ArticleStore('db.pricemesh.com', 27017)
    print store.next_id()